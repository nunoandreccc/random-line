# random-line

Random Line
This library returns a random line for a given file, but you must provide your
own file.

Preparing your environment

Install Node and npm

Create an npm account here:
Node Package Manager

Go to GitLab and create an account.


Pushing your code there

Create a new repository, and push this code there.
run npm login

run npm init

Make sure to include your username on the package name. Example:
@melomario/random-line


A wild new file appears, and it's called package.json.